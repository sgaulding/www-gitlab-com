---
layout: markdown_page
title: "FY21-Q3 OKRs"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from August 1, 2020 to October 31, 2020.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2020-06-30 | CEO shares top goals with E-group for feedback |
| -4 | 2020-07-06 | CEO pushes top goals to this page |
| -3 | 2020-07-13 | E-group pushes updates to this page |
| -2 | 2020-07-20 | E-group 50 minute draft review meeting |
| -2 | 2020-07-22 | E-group discusses with teams |
| -1 | 2020-07-27 | CEO reports give a How to Achieve presentation |
| 0  | 2020-08-03 | CoS updates OKR page for current quarter to be active |
| +2 | 2020-08-17 | Review previous and next quarter during the next Board meeting |

## OKRs

### 1. CEO: IACV
[Epic 720](https://gitlab.com/groups/gitlab-com/-/epics/720)
1. **CEO KR:** Implement 5 initiatives in sales to increase [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) by $XM.
2. **CEO KR:** Achieve a [CAC Revenue Payback Ratio](https://about.gitlab.com/handbook/sales/#cac-to-revenue-payback-ratio) < $X.
    1. CRO: Deliver three pilot programs to increase iACV / decrease Sales and Marketing expense
3. **CEO KR:** Double down on what works in marketing. Only do marketing activities that >X CWlinear to spend. 

### 2. CEO: Popular next generation product
[Epic 722](https://gitlab.com/groups/gitlab-com/-/epics/722)
1. **CEO KR:** Continue winning against Microsoft Azure GitHub. Improve our competitive win rate from X to Y. 
2. **CEO KR:** Drive total monthly active users, [TMAU](https://about.gitlab.com/handbook/product/performance-indicators/#total-monthly-active-users-tmau) and [Paid TMAU](https://about.gitlab.com/handbook/product/performance-indicators/#paid-total-monthly-active-users-paid-tmau), from X to Y.
3. **CEO KR:** Complete three key packaging projects.

### 3. CEO: Great team
[Epic 721](https://gitlab.com/groups/gitlab-com/-/epics/721)
1. **CEO KR:** Exceed 50% top of funnel from outreach and 90% of outreach to diverse candidates.
2. **CEO KR:** Certify 25 non-engineering team members in self-serve data via handbook first materials.
3. **CEO KR:** Certify 25% of people leaders through manager enablement certification.

## How to Achieve Presentations

* Engineering
* Finance
* Legal
* Marketing
* People
* Product
* Product Strategy
* Sales
