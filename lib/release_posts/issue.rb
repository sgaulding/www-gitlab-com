# frozen_string_literal: true

module ReleasePosts
  class Issue
    STAGE_PREFIX = 'devops::'
    GROUP_PREFIX = 'group::'
    CATEGORY_PREFIX = 'Category:'

    def initialize(url)
      @url = url

      uri = URI(url + '.json')
      response = Net::HTTP.get(uri)
      @issue = JSON.parse(response)
    end

    def title
      @issue['title']
    end

    def labels
      @issue['labels'].map { |label| label["title"] }
    end

    def available_in
      available_in = ['ultimate']
      return available_in if labels.include? 'GitLab Ultimate'

      available_in.unshift('premium')
      return available_in if labels.include? 'GitLab Premium'

      available_in.unshift('starter')
      return available_in if labels.include? 'GitLab Starter'

      available_in.unshift('core')
    end

    def group_label
      labels.find { |label| label.start_with? GROUP_PREFIX }
    end

    def stage_label
      labels.find { |label| label.start_with? STAGE_PREFIX }
    end

    def stage
      stage_label.delete_prefix(STAGE_PREFIX)
    end

    def categories
      category_labels = labels.select { |label| label.start_with? CATEGORY_PREFIX }
      category_labels.map { |label| titleize(label.delete_prefix(CATEGORY_PREFIX)) }
    end

    attr_reader :url

    private

    def titleize(string)
      string.split.each(&:capitalize!).join(' ').to_s
    end
  end
end
