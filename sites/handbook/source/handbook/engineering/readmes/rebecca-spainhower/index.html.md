---
layout: markdown_page
title: "Rebecca Spainhower's README"
job: "Support Engineering Manager"
---

### My Role and Responsibilities
For a quick summary of everything I feel is important as a Support Engineering Manager, please see [this article](https://circleci.com/blog/how-engineering-managers-can-effectively-support-engineers-teams-and-organizations/#) by [Lena Reinhard](https://www.linkedin.com/in/lenareinhard/), VP of Product Engineering at CircleCI. 

### Tips on Excellent Management
[A new manager's guide to growing into your role](https://www.atlassian.com/blog/leadership/new-manager-tips) -- from Atlassian blog on Leadership

### Questions I Ask Myself
These questions are drawn from Gay Hendricks' book [The Big Leap](https://www.goodreads.com/book/show/6391876-the-big-leap):
* What work do you do that doesn't seem like work?
  - career development - for teammates and myself
  - process improvements - thinking systematically and asking questions about alternatives
  - document reviews and revamps (I love to read!)
* In your work, what produces the highest ratio of abundance and satisfaction to the amount of time spent?
  - 1:1 meetings with teammates - building connection
  - creating processes, documents, and guidelines that enable others to achieve work goals
  - any activity that brings together diverse viewpoints and geographically distributed teammates
* What is your unique ability?
  - fostering team camaraderie that endures over time
