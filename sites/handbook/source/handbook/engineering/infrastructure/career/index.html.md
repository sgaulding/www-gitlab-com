---
layout: handbook-page-toc
title: "Career Development"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Tools

There are a number of tools we use to plot and manage career development:

* Role descriptions, which outline role responsibilities, requirements and nice-tohaves for each level in the role.
  * [DBRE](/job-families/engineering/database-reliability-engineer/)
  * [SRE](/job-families/engineering/site-reliability-engineer/)
  * [Backend Developer, Delivery](/job-families/engineering/backend-engineer/)
  * [Backend Developer, Infrastructure](/job-families/engineering/backend-engineer/)
* [Compa Ratios](/handbook/total-rewards/compensation/compensation-calculator/#compa-ratio), which reflects demonstrated ability to drive projects and deliverables from the position description in a quantifiable manner.
* Quarterly checkpoints
* 1:1s
* [360 Feedback](/handbook/people-group/360-feedback/)

## Workflow

* **Compa Group**: All GitLab team-members should have an Compa Group which reflects their current level's Compa Ratio.
* **Quarterly Checkpoints**
  * On the first 1:1 meeting of the quarter, discuss the EFW and select two or three role responsibility areas to focus on during the quarter. Make a note of them in the 1:1 document and add them to the standing agenda. For each area, outline specific goals and targets to achieve, as well as ideas to work on them.
  * On the last 1:1 meeting of the quarter, discuss and record a summary of the progress made for the quarter and update the EFW accordingly.
* **1:1s**: Mentor on the focus areas in weekly 1:1s at a frequency that feels right for the tasks. There is no hard rule to discuss the career development items every week, but try to do so regularly and avoid a wide-open, ad-hoc conversation.

There is no rule that guides the amount of progress that should be made in a given time-period in a strict fashion. We should however strive to set targets to progress to the [next level](/handbook/total-rewards/compensation/#explanation) on at least a quarterly basis.

Actions to make changes to a GitLab team-member's level can be taken during the [360 Feedback](/handbook/people-group/360-feedback/), and the data collected throughout this workflow should be useful at that time.

### Managers

You must prepare to provide mentoring and guidance on focus areas for the quarter, especially in terms of how to improve and evolve in said areas. Do not hesitate to ask for help if you feel you need guidance to assess strategies for mentorship, and feel free to reach out to the other GitLab team-members who can provide said guidance. Be clear and concise in setting expectations, trying to align them with work that will take place during the quarter.

### Individual Contributors

Think about the areas that are important to you and commmit to working on them over the quarter. Keep an open mind to receive feedback and guidance on these areas, and be an active participant in the process.

## Guidelines and Considerations

Career development requires that all parties involved be committed to the process and are active participants.

One implicit area to focus on explicitly is **communication**. Regardless of your role, whether tehnical or manager, you must be a good communicator, and thus, it is important to always improve our communication skills.

While there is no time-boxing for level progress, the company does operate on a [specific cycle](/handbook/people-group/360-feedback/) to make changes to a GitLab team-members level during the [360 Feedback](/handbook/people-group/360-feedback/).


## Interning with Infrastructure / Reliability Engineering

Cross reference for [Interning for Learning](/handbook/people-group/promotions-transfers/#interning-for-learning)

There is a reasonable amount of interest to intern with Infrastructure so we wanted to put a brief landing section in the handbook.  If you are interested in interning with Infrastructure, we would ask that you talk to your current manager and create an issue per the mentioned process.  This can help us track who is helping on what and what they would like to learn.  Mention the Reliability Engineering team members and managers to help us know you are interested.  One note, in some cases, you may need to submit access-requests to get access to certain systems to help with work.

Don't know what to learn? Talk with us! We have a broad set of technical skills listed in the [job description for SRE](/job-families/engineering/site-reliability-engineer/) if you are looking for ideas on what to learn.  Set up a coffee chat with a team member or one of the managers.
If you're interested in learning more about SREs in general, you may wish to read Alice Goldfuss's blog post, "[How to Get Into SRE](https://blog.alicegoldfuss.com/how-to-get-into-sre/)." This blog post is an overview of all sorts of SRE related things, some of which may not be in use here at GitLab. However, it can be a valuable tool to learn more about SREs in general, and it links to many resources that you can use to learn more about a variety of SRE related topics.

Specific ways to work in the internship:
1.  Pair up with an engineer on a particular issue or for our project work in OKRs.
2.  Shadow the On-Call in your similar timezone for 2 weeks
3.  Shadow the Delivery team to learn more about how we release to GitLab.com
