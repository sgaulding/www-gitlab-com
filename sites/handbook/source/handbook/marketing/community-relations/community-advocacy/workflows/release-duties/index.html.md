---
layout: handbook-page-toc
title: "Release Day Duties"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Every 22nd of the month we release a new version of GitLab. More often than not we get a spike in community mentions. To help deal with this we have dedicated release advocates that own the effort of involving experts in community mentions on/after a release.

The two channels that we see the biggest increases in mentions and discussions are:

* [The GitLab blog](/blog/)
* [HackerNews](https://news.ycombinator.com/news)

Advocates rotate as the release day DRI on a monthly basis. If the release day takes place on a weekend, one of the advocates is assigned to monitor the traffic and to process mentions. We keep track of the assignments on the `Community Advocates` GitLab team calendar.

On release day, the best place to go for relevant information is the [#release-post Slack channel](https://app.slack.com/client/T02592416/C3TRESYPJ). The channel description of this channel is updated each month with links to the relevant preview page and review app.

### Responsibilities of the advocate DRI Include:
* Connecting with release day managers leading up to release
* Communicating important changes and information back to advocates team
* Compiling relevant expert outreach data
* Attending release retrospective meeting. If this meeting is not within advocates working hours, it is fine to add notes and data to relevant retrospective issues instead.

Note: It is not the sole responsibility of the advocate DRI to take on all expert outreach for related relase day mentions, but instead, serve as the main point of contact between relase day managers and the advocates team.


## Action Items for Release Day Advocate DRI

On the 15th of the month, a reminder message is generated using the Slack bot to the [#advocates-fyi Slack channel](https://app.slack.com/client/T02592416/C016JETG68Y/thread/CB16DMSLC-1594796408.481100), as a reminder for the advocate DRI to review and take action on the following tasks.

### 1 Week Prior to Release Day Tasks

1. Confirm advocate as the DRI for Release Day. The team should consider choosing coverage for 2-3 months ahead of time during daily planning calls to allow for coverage planning.
1. Before the 18th of the month, review the Preview Page, linked in the channel description of the [#release-post Slack channel](https://app.slack.com/client/T02592416/C3TRESYPJ). This will have the most up to date information regarding the next release
1. After the 18th, review the Release Post Blog View App, also linked in the channel description of the [#release-post Slack channel](https://app.slack.com/client/T02592416/C3TRESYPJ) for a finalized list of what will be included in the release.
1. The author of this blog post is the Release Day Manager
1. Reach out to the Release Day Manager in the #release-post Slack channel to confirm they understand the expert outreach process, and to expect pings around the release from the community advocates.
1. Collaborate with the Tech Evangelism team for their 'Release Evangelism' assignments, and understand what questions and topics they can respond to
1. If the content of the release is anticipated to generate an increase in questions, consider engaging with an [Advocate-for-a-day](#advocate-for-a-day) in advance.


### Release Day Tasks

On release day, a reminder message is generated using the Slack bot to the [#advocates-fyi Slack channel](https://app.slack.com/client/T02592416/C016JETG68Y/thread/CB16DMSLC-1594796408.481100), as a reminder for the advocates to watch for an increase in mentions, and to utilize the #release-post channel for expert outreach.

#### Zendesk Tickets

- When working a Zendesk ticket that relates to release day, check the "Release Day" box in the left pane of Zendesk before closing out the ticket. This will help the team determine how many Zendesk cases on release day are related to the release. The Community Advocates can leverage this information to ensure that product managers and release managers are able to help with cases on release day.

#### Expert Outreach

- Reach out to release day managers in the #release-post Slack channel with the following template for expert involvement.

`Hi @expert_username! [LINK TO COMMUNITY COMMENT] An expert is needed to respond to this question/comment about the current release. Could you please answer on [name of social platform] using your own individual account?  If you don't know the answer, could you share your thoughts and ping a specific release day expert who might? Thanks!`

- Note that PMs should have a strong overview of who is the best expert for each new feature in the release and can help us find appropriate team member experts for each release.
- The Principal Product Manager can also support routing expert outreach to the correct manager or PM, and can support expert involvement if advocates feel that they can't get a timely response.
- Copy the overview of the main features or improvements from the beginning of the release blog post and post it to the HackerNews story that comes out during the release. You can use the [11.8 summary post](https://news.ycombinator.com/item?id=19228781) as an example.
- Monitor the `#release-post` Slack channel throughout the day to be ready at the time the release blog post is published.
- If you are on release duty and will be offline for a period of time, let another advocate know so they can monitor.


### 1 Week Post Release Day Tasks

The advocate DRI will run a Zendesk report with the Release Day ticket data prior to the release retrospective meeting and deliver that information to stakeholders at the meeting.

Notes: If this meeting is not within advocates working hours, it is fine to add notes and data to relevant retrospective issues instead.

#### Expert Involvement on Release Day Report
1. In Zendesk, navigate to the `Reporting` tab
1. On the `Insights` -> `Tickets` tab, hover over the `Tickets Created` metric and click the small dropdown arow that appears.
1. Select `View this Report`
1. Click on the `Filter` tab.
1. Click `Add Filter` and choose the `Select from a List of Values` option. For the first attribute, choose `Release Day`, and for the second attribute, select `True`
1. Select the `Table` report option
1. Take a screenshot or download the data table.

#### Retrospective Meeting
1. At least one representative from the Community Advocates team should attend the Retrospective meeting following release day to share and reflect on expert involvement data
1. Location the relevant release day retrospective issue to provide feedback and data asynchronously.


