---
layout: handbook-page-toc
title: "GitLab Giveaways"
---

## What is a Swag Giveaway?

GitLab Swag Giveaways are an opportunity to share GitLab Swag with members of our wider community. During a giveaway, unique redemtion links are sent to community members where they can select and directly ship themselves swag.

Examples of when a GitLab teams might host a giveaway could include:
*  Recognition of 5 contest winners
*  Completion of a survey by the first 300 participants
*  Participiation in a challenge

GitLab Giveaways are meant to serve as recognition and celebration of a specific group within our community. If you're looking to recognize an individual community member for their contributions, please use our [Nomination for Community Swag process](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/community-rewards-internal/).



## Purpose

The goal of this guide is to clearly outline the timeline and action steps GitLab teams and Community Advocates can expect while planning a swag giveaway.

## Giveaway Scenarios

Any team at GitLab can use Printfection to execute a swag giveaway to our wider GitLab community. 

Scenarios where you might use a Printfection giveaway:

| Scenario | Description |
| ----- | ----- |
| Bulk Swag: giveaways that use bulk swag items currently in stock in our Printfection warehouse. | You want to send existing swag from the GitLab store. |
| Custom Swag: giveaways that use swag created specifically for the giveaway | You have, or plan to create, custom swag that you'd like to send to the community. |


## Bulk Swag Giveaways


### Timeline

This is the best option if you'd like to execute a giveaway quickly. Items are already in the Printfection warehouse and ready to be used right away.

* 1-2 weeks before the giveaway: connect with the Community Advocates team for the most up to date inventory totals of bulk swag items.

## Custom Swag Giveaways


### Timeline

It takes more time to complete a custom swag giveaway. Please remember to factor in the time it takes to [order new swag](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#ordering-new-swag) while planning your giveaway.


| Timeline | Action |
| --- | --- |
| T - 4 weeks | Ship your custom swag to Printfection. This allows for 1 week for shipping to the warehouse. This is also the best time to communicate with the Community Advocates, for both the warehouse address and to start communication around support for the giveaway |
| T - 2/3 weeks | Plan for Printfection to take between 1 and 2 weeks to process all new swag items into the warehouse. The swag will not be ready to send until it is processed in their warehouse. |
| T - 1 week | Build giveaway campaigns in Printfection and coordinate with Community Advocates on sending giveaway links |


## Setting up a Giveaway in Printfection

If you need to create custom swag, start with step 1.  
If you already have your custom swag, start with step 3.  
If you're using swag from the GitLab store, start with step 6.

1. [Choose a vendor](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#choosing-a-vendor) who will produce your swag. Please note that using Printfection may result in faster turnaround, as it will cut out extra shipping and processing.
2. [Create your swag request](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#creating-replenishing-and-ordering-new-swag-items)
3. [Send your swag to the Printfection warehouse](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#send-items-to-printfection-warehouse) and [add swag as a new item in Printfection](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#add-new-item-to-printfection).
4. Create a [customer sourced order](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#creating-the-send-order) in Printfection.
5. Wait until items are added to the Printfection warehouse to proceed. You do not need to wait until the inventory is added to the warehouse, but just until the items are added as options in the portal.
6. If you're sending only one swag item, or you'd like to give people the choice to choose 1 out of a few items, create a new [giveaway campaign](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#create-a-new-giveaway-campaign) and add your custom swag to the campaign. Check what swag is available for a giveaway by looking items with the 'Giveaway Option' tag in the product name in the [Printfection Inventory](https://app.printfection.com/account/inventory/inventory_list.php)
7. If you're sending more than one swag item, [create a kit](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#creating-giveaway-kits), then create a [giveaway campaign](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#create-a-new-giveaway-campaign) and add your kit to the campaign.
8. Confirm that your swag inventory is physically available in the Printfection warehouse before starting your Giveaway.
9. Your giveaway is now ready to go! Follow the steps below for Sending Giveaway Links


## Sending Giveaway Links

In Printfection, we generate unique links for each person receiving giveaway swag. We use unique Printfection links instead of coupon codes in the Shopify store to reduce abuse of the giveaways. 

Make sure to arrange the logistics around sending giveaway links with advocates before the campaign started. This needs to be planned ahead in order to ensure that advocates have the bandwidth to support that giveaway.

Options for how to send giveaway links:
* Email
* Twitter DM

## Closing Out your Giveaway

It's important when your giveaway is over to correctly close out your giveaway in Printfection to avoid additional item storage charges.

1. Follow the steps to [delete your campaign in Prinfection](https://help.printfection.com/hc/en-us/articles/114094187834-How-to-delete-a-campaign)
1. If you've used all your swag, follow the steps to [archive your items](https://help.printfection.com/hc/en-us/articles/216948387-How-to-archive-items).
1. If there is remaining swag, either ship the swag to your preferred address, or work with the community advocates team to see if the swag could be used for our wider community or Shopify store.
1. Remember that failure to remove excess swag or archive your items before the start of a new month will result in an additional $25 month charge per item for storage.


## Related Costs

Below are the related costs in addition to the cost of actuals swag that you should plan for while planning your giveaway.

### Item Storage in Prinfection

$25 per item, per month.

For example: If you ship 100 hoodies, 2000 stickers, and 1 speaker, the storage cost will be $75 per month. If you ship 1 hoodie, 1 sticker, and 1 speaker, the storage cost will still be $75 per month.

Please note that you must store your items in Printfection for the entire length of your giveaway, meaning the entire time it takes to share codes, fulfill, and ship orders.

### Customer Sourced Orders

There is a receiving fee for shipping and processing swag from a different vendor to the Printfection warehouse. Please review the [Printfection documentation regarding receiving fees](https://help.printfection.com/hc/en-us/articles/115002120194-Understanding-customer-sourced-inventory) for an accurate estimate.

### Fulfillment and Shipping Costs

Giveaway fulfillment and shipping costs varies by item size, cost, and location of shipping. Please review [Printfection's example shipping and fulfillment costs](https://help.printfection.com/hc/en-us/articles/204467034-Example-shipping-fulfillment-costs) for an accurate estimate.





