---
layout: handbook-page-toc
title: "Reviewing merge requests"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

**This page is related to `about.gitlab.com` and the [GitLab handbook](/handbook/).**

Merge requests are an important process where we peer review incoming changes to ensure code and content meets our standards and does not break any existing functionality.

In our continuous integration environment, people with merge permissions are the final check before content is released. Please use this privilege responsibly and ensure that merge requests align with our [GitLab values](/handbook/values/).

## Always look for

1. Do the changes impact what they are supposed to impact as expected?
1. Do the changes impact something they are NOT supposed to impact?
1. Check all relevant browsers.
    * Relevant browsers are defined as those a significant percentage of our end-users utilize which have a different underlying engine.
        * Chrome (Blink), Firefox (Gecko), and Safari (Webkit).
1. Check all relevant device resolutions.
    * If this is an email, social, or ad campaign it's likely going to be viewed first on a mobile device.
1. Are any resources are appropriately sized and compressed?
1. Are our [guidelines](#guidelines) being followed?

## Regression risks

### Overall

1. Does the cookiebot (GDPR, CCPA, etc) behave as expected?
    * Please review from a relevant geolocation via VPN and ensure related code behaves appropriately.
1. Are forms behaving as expected?
    * Before submitting the form?
    * While submitting the form?
    * After submitting the form?
1. Does search work as expected...
    * In the site header navigation?
    * In the handbook search box?
    * On the blog?

### The about website

1. Do pages and flows related to core business aspects behave as expected?
    * /pricing/
    * /free-trial/
    * /sales/
    * /support/

### Handbook

1. Is the table of contents generating as expected?
1. Are the section links generating as expected on heading elements?
1. Does the benefits (salary) calculator work?
1. Are data driven (YML) items being inserted into related handbook pages as expected?
1. Do Mermaid graphs work as expected?
1. Are Sisense (Periscope) charts working as expected?
1. Does embedded third party code such as youtube videos or calendars work as expected?

### Blog

1. Does the blog homepage generate as expected?
1. Do the blog posts generate as expected?
1. Are the rss feeds generated as expected?
1. Do the newsletter subscription forms work as expected?

### Releases

1. Does the releases page generate as expected?
1. Do the release posts generate as expected?
1. Are the rss feeds generated as expected?

## Code

1. Is the code well written?
    * Note that "well written" is relatively subjective but there are industry best practices such as [DRY code](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself), efficient code, and secure code.
1. Does this code expose or implement any PII or other security related items?
1. Are any variables, constants, code parameters, and other items named appropriately?
1. Is the code organized logically?
1. Is code commented where appropriate?
1. Are functions appropriately sized? Are they small or monolithic?
1. Will or should this code impact any automated tests? If so, do they work as expected?
1. Are there any new javascript console errors or warnings introduced?
1. Please disable any console logs before merging.

## Copy

1. Are there any typos in the copy?
1. Are our [guidelines](#guidelines) being followed?

## Conversion

1. Does this follow best practices for conversion?
    * TODO: documentation link

## Design

1. Does this follow our Brand [guidelines](#guidelines)?
1. Do submitted images support our company values of [diversity, inclusion, and belonging](/handbook/values/#diversity-inclusion)?
1. Are submitted images and other resources appropriately sized and compressed?
1. Are images sufficiently clear and not blurry?

## UX

1. Does this follow best practices for UX?
    * TODO: documentation link

## Accessibility

1. Does this follow best practices for accessibilty?
    * TODO: documentation link

## Social media

1. TODO: documentation link.
1. Are the elements necessary for social media present?
    * Opengraph image?
    * Appropriate title and description?

## SEO & analytics

1. Does this follow best practices for SEO?
    * TODO: handbook documentation link
1. Has SEO analysis been done? Are the required elements present?
1. Will this change impact metrics in a meaningful way?
    * Are the required analytics being gathered as expected?
    * Are the teams impacted by these metrics aware of the upcoming changes?

## Experiments

1. Are the A/B tests behaving as expected?
    * On the control experience?
    * On the test experience?
    * Are the necessary analytics being gathered to measure impact of the test?

## Guidelines

* [GitLab Engineering's style guidelines](https://docs.gitlab.com/ee/development/contributing/style_guides.html).
    * The about website doesn't enforce the same code standards as GitLab engineering's product development style guides however it's recommended to follow these guidelines when possible.
    * [CSS guidelines](https://docs.gitlab.com/ee/development/fe_guide/style/scss.html).
    * [Javascript guidelines](https://docs.gitlab.com/ee/development/fe_guide/style/javascript.html).
    * [Ruby guidelines](https://docs.gitlab.com/ee/development/contributing/style_guides.html#ruby-rails-rspec)
* [GitLab Engineering's code review handbook](/handbook/engineering/workflow/code-review/).
* [Marketing writing style guide](/handbook/marketing/growth-marketing/content/editorial-team/#blog-style-guide).
* [Marketing naming conventions](/handbook/marketing/website/#naming-conventions).
* [Marketing tone of voice](/handbook/marketing/corporate-marketing/#tone-of-voice-1)
* [Brand guidelines](/handbook/marketing/growth-marketing/brand-and-digital-design/brand-guidelines/).
