---
layout: handbook-page-toc
title: "Technology Partner Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Who are we

Partner Marketing (includes both technology partners and channel partners) is the single connection point from alliances and channel to PMM, PM, FMM, corporate events, digital marketing and marketing programs. We ensure joint partner/channel messaging, joint value proposition and focus on the current and future scope for both organizations. Our main goal is to leverage our strategic partners to increase awareness of GitLab while increasing joint sales for our enterprise users.

<i class="fas fa-users fa-fw color-orange font-awesome" aria-hidden="true"></i> **The Team**

- [**Tina Sturgis**](https://about.gitlab.com/company/team/#TinaS), 
Manager, Partner and Channel Marketing **{Focused on AWS, Channel Marketing interim, Commit partner sponsorships, & overall Partner and Channel Marketing Strategy}**

- [**Alisha Rashidi**](https://about.gitlab.com/company/team/#Arashidi), 
Partner and Channel Marketing Manager **{Focused on Google Cloud, VMware and AWS}**

- [**Sara Davila**](https://about.gitlab.com/company/team/#saraedavila), 
Senior Partner and Channel Marketing Manager **{Focused on Eco-partners, RedHat, Azure & HashiCorp}**


This page will cover all of Technology Partner Marketing. If you need information for Channel Partner Marketing, [click here](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/channel-marketing/). 


## <i class="fab fa-gitlab fa-fw color-orange font-awesome" aria-hidden="true"></i> What We Do 

- Drive awareness, demand generation and influence joint revenue by way of executing GTM programs with [cloud and platform providers, and integration partners](https://docs.google.com/spreadsheets/d/1-EE7vChGkDeyJxoM-LjVmUdwYwboxBmq8_42hjHGw_w/edit?usp=sharing). 
- Evaluates and fills the gaps of joint content and collateral for Tier 1 and ecosystem partners.
- Maximize partner MDF programs to participate and promote partner specific events, webcasts, content initiatives, etc.
- Funnels in income from partner sponsorships for virtual and physical events.

## 🚧 ️What are we working on
Check out our [Tech Partner Marketing board](https://gitlab.com/groups/gitlab-com/-/boards/1548536?label_name[]=Alliances&label_name[]=Partner%20Marketing) with all the details of what we have inflight

## How to Request Partner Marketing Support
*Partner Marketing executes:*
* Partner hosted webcasts
* GitLab hosted webcasts
* Partner guest blogs
* Updates to partner solution webpages
* Events
* Joint content creation
* Campaigns (tech partner and/or channel related)
* And More

**How To Engage Partner Marketing:**

*Partner Marketing's inital issues response SLA is 48 hours.*
To start the Partner Marketing support process, 
* [Create an issue](https://gitlab.com/gitlab-com/marketing/partner-marketing/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) 
* This issue template `tech_partner_request` is available in the Partner Marketing project on gitlab.com 
* Complete all sections of the issue template and submit...next steps are stated in the issue

If you are requesting a technology partner webcast or event, please make sure that the partner has the following:
* An integration with GitLab
* A joint solution that maps to a current GTM use case
* At least one joint customer/customer success story


## Partner Marketing Webinar Process

### **GitLab Hosted** Partner Webinars <br>
*(Typically we host 2 webcasts per month with GitLab Partners)*

**Step 1: Creating a GitLab webcast** 

* Identify partner, topic, and a potential date for webcast
* Create the copy for the title and abstract (in collaboration with the partner, and any other speakers) and gain approval, and obtain sign off from the partner.
* Create the copy for the landing page and provide speaker bios and, if applicable, photos of the speakers.
   + *If we are sharing the leads with the partner, partner marketing works with the partner to identify requirements, and will open an issue for Marketing Ops to provide the lead list post-event.*
* Create the copy for the invite emails, and determine how many invite emails will be sent pre-webcast. 
  + *MPM and Partner marketing: reviews emails before sending*
* Within the main issue of the webcast, identify the target audience, previous campaigns, and previous events for the lead list for the invitation emails.
* MPM is responsible for setting up [calendar invites for kick-off call, content reviews, dry run and webcast](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/#project-planning)

**Step 2: Pre-webcast work** 
* If applicable, work with digital marketing (paid advertising), MPM on where the budget coming for paid advertising will come from.
  + *Provide guidance on targeting which includes but is not limited to job titles to target and twitter accounts and hashtags recommended.*
* Set up SDR outreach issue with SDR outreach template in the Product Marketing Board.
  + *Tag [global SDR managers and SDR enablement manager](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/)*
  + *Post the issue in the #sdr_global slack channel for visibility* 
* Work with MPM to set up organic social promotion with the [social marketing team](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/).

**Step 3: Executing the webcast**
* MPM acts as the project manager and the moderator for the webcast
  + *Make sure the slides and and that the webcast is made available as on-demand asset post webcast*
* If applicable, [set up a swag link](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/community-rewards-internal/) to give to the partner presenter as a thank you.

**Step 4: Post-event follow up**
* Work with the MPM on the follow up email for attendees and no shows of the webcast. The follow-up email(s) should be prepped and ready to go (minus links to the recording) 48 hours prior to the live event. The following is minimum guidance for what should be included in the follow-up email:
  + *Slide Deck and Unlisted Youtube video of the webcast*
  + *A call-to-action for a 30-day trial, any other relevant joint partner collateral (gated or not), and/or an applicable Path Factory*
  + *Follow up emails should be sent 24-48 hours post webcast*
* Work with the MPM on the [conversion of the on-demand page post webcast](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/#converting-the-webcast-to-an-on-demand-gated-asset)
* If we are sharing leads with the partner, partner marketing works with MPM and Marketing Ops to share the leads.

**Step 5: Reporting**
* Fill out lead and salesforce reporting in the [partner and channel webinar tracker](https://docs.google.com/spreadsheets/d/1eoT3i8PO-YZdsoLJn4FIGtLPzo-r-fQbRp91oMKgM2Y/edit#gid=1732141776)

### Partner-Hosted Webinars

*(Typically we provide, high-level support, for two partner-hosted webcasts per month for GitLab Technology Partners)*


**Step 1: Creating a Partner-hosted webcast**
* Partner will reach out to GitLab Partner Marketing or Ecosystem lead with the request for a webinar and identify their speaker, potential topic, and date for the webinar
* Together with the hosting partner and both parties speakers, GitLab Partner Marketing will iterate on the copy for the title and abstract and bring it to a place where all parties are satisfied.
* The hosting partner will then create the landing page based on the agreed upon title and abstract.
*GitLab Partner Marketing does not do a database email send for partner-hosted webcasts. However, the hosting partner may create the copy for the invite emails that will be sent on our behalf*
* The hosting partner is responsible for project management of the webcast (ie. setting up and sending up all calendar invites for):
  + kickoff call
  + content reviews 
  + dry run 
  + webcast

**Step 2: Pre-webcast work**
* Kickoff Call
  + Hosting partner will host a kick off call with:
    + All Speakers
    + Both companies marketing point of contacts (POCs)
  + In this call, the parties will align, at a high level, on the action points in the Partner Hosted webinar template. The purpose of this is to ensure that all action items, needed to execute the webinar and following meetings, are discussed and assigned and that all parties are aware of next steps.
* During this time, GitLab Partner Marketing will open the following issue:
  + Marketing Ops and MPM - for post event [`event-clean-upload-list`](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#), if applicable
  + Social - for pre event [`social-general-request`](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#), if applicable
  
**Step 3: Executing the webcast**
* The hosting partner acts as the deliverer of the action items, as well as, the moderator for the webcast

**Step 4: Post-event follow up**
* On a case by case basis - GitLab MPM to make the MP4 available as on-demand, gated, asset post event and add to Path Factory
* Hosting partner will share the leads with GitLab Partner Marketing, if agreed upon earlier.
* GitLab Partner Marketing will then clean and share the lead list with MPM 
* GitLab MPM will work with Marketing Ops to upload lead list and select the nurture campaign to put them in

**Step 5: Reporting**
* GitLab Partner Marketing will fill out lead and salesforce reporting in the partner and channel [webinar tracker](https://docs.google.com/spreadsheets/d/1eoT3i8PO-YZdsoLJn4FIGtLPzo-r-fQbRp91oMKgM2Y/edit#gid=1732141776)


